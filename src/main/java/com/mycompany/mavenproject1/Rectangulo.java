
package com.mycompany.mavenproject1;


public class Rectangulo extends FiguraGeometrica{
    private double base;
    private double altura;

    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    
    // METODO PARA HAYAR AREA DEL  RECTANGULO
    public double area()
    {
    return base*altura;
    }
    
    
    
    
    // METODO PERIMETRO RECTANGULO
    public double perimetro()
    {
    return (base*2)+(altura+2);
    }

    
    // METODOS SETTERS Y GETTERS 
    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
}
