package com.mycompany.mavenproject1;


public class Triangulo extends FiguraGeometrica{

private double base;
private double altura;
private double l1, l2, l3;



    public Triangulo(double base, double altura, double l1, double l2, double l3) {
        this.base = base;
        this.altura = altura;
        this.l1 = l1;
        this.l2 = l2;
        this.l3 = l3;
    }

    
@Override

 // METODO HALLAR AREA AL TRIANGULO
    public double area()
    {
        return (base*altura)/2;
    }

@Override
     
// METODO PARA HAYAR  PERIMETRO TRIANGULO
    public double perimetro()
    {
        return l1+l2+l3;
    }

    
    
    
    // SETTERS Y GETTERS 
    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getL1() {
        return l1;
    }

    public void setL1(double l1) {
        this.l1 = l1;
    }

    public double getL2() {
        return l2;
    }

    public void setL2(double l2) {
        this.l2 = l2;
    }

    public double getL3() {
        return l3;
    }

    public void setL3(double l3) {
        this.l3 = l3;
    }
    
    
    
}
