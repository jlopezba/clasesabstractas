


package com.mycompany.mavenproject1;

 /*definimos mi clase abstraccta.
  una clase abstracta es una clase que tiene metodos que no pueden ser desarrollados por falta de informacion concreta.
  y deben desarrollarsen en las sub-clases, cuando esta informacion ests disponible.
 */

public abstract class FiguraGeometrica {
    
    // metodos abstractos para hallar area y perimetro a figuras.
    public abstract double area();
    public abstract double perimetro();
    
    @Override
    
   // definimos a mi metodo ToSring
    public String toString ()
    {
        return "área: "+ area()+ "\n"+ "Perímetro: "+ perimetro() ;
    }
            
    }
