
package com.mycompany.mavenproject1;
 
// aqui estamos diciendo que circulo EXTENDS, extiende de figuras geometricas.
public class Circulo extends FiguraGeometrica{
    
    // atributos del circulo
    private double radio;

    
   
    public Circulo(double radio) {
        this.radio = radio;
    }
    
    @Override
    
    // METODO PARA HAYAR AREA CIRCULO
    public double area()
    {
        return Math.PI*Math.pow(radio, 2);
    }
    
    
    
    // METODO PARA HALLAR  PERIMETRO DEL CIRCULO
    public double perimetro()
    {
        return (Math.PI*2)*radio;
    }

    
    // METODOS GETTERS Y SETTERS 
    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
    
    
}
