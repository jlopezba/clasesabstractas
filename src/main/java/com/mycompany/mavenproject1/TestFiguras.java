package com.mycompany.mavenproject1;


public class TestFiguras {
    public static void main(String[] args) {
        
      //SE CREAN OBJETOS A CADA UNO DE LAS FIGURAS PARA INSTANCIAR, LLAMAR,INVOCAR
      //LOS METODOS QUE SE INPLEMENTARON EN CADA FIGURA.
        
        Circulo circ = new Circulo(3);
        Triangulo triang =new Triangulo(3, 3, 4, 5, 6);
        Rectangulo rect =new Rectangulo(3, 3);
        
      // AQI MANDO A IMPRIMR A MOSTAR POR PANTALLA MIS RESULTADOS DE LAS OPERACIONES PLANTEADAS EN CADA UNO DE LOS METODOS.
        
         System.out.println("\n");
        System.out.println("##### PERIMETRO Y AREA DEL CIRCULO #####");
        System.out.println(circ);
        System.out.println(" \n");
        System.out.println("##### PERIMETRO Y ARAE DEL TRIANGULO #####");
        System.out.println(triang);
        System.out.println(" \n");
        System.out.println(" ##### PERIMETOR Y ARAE DEL RECTANGULO #####");
        System.out.println(rect);
        System.out.println("\n");
        
    }
    
}
